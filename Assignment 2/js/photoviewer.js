var images = ["images/0004681.jpg", "images/0004755.jpg", "images/0004858.jpg", "images/0004902.jpg", "images/0006630.jpg", "images/0004713.jpg", "images/0004801.jpg", "images/0004860.jpg", "images/0004931.jpg", "images/0006638.jpg", "images/0004720.jpg", "images/0004802.jpg", "images/0004870.jpg", "images/0004969.jpg", "images/0006680.jpg", "images/0004731.jpg", "images/0004827.jpg", "images/0004874.jpg", "images/0004971.jpg", "images/0006743.jpg", "images/0004750.jpg", "images/0004853.jpg", "images/0004888.jpg", "images/0006598.jpg", "images/0006787.jpg"];
var grid = document.body;;
var activeZoom;

function createGallery() {
    for(var i = 0; i<images.length;i++) {
         var container = document.createElement("div");
         container.setAttribute("class", "container");
         grid.appendChild(container);
         importImage(images[i],null,i);
    }
 }

function importImage(src,alt,number) {
	var element, newImage, container;
	newImage = new Image();
	newImage.src = src;
    newImage.alt = alt;
	newImage.setAttribute("onclick","clicked(this);");
	newImage.setAttribute("class", "normalImage");
	element = document.getElementsByClassName("container")[number];
	element.appendChild(newImage);
}

function clicked(image) {
	if (image.getAttribute("id") === "zoomedImage") {
		image.setAttribute("class", "normalImage");
        var element = document.getElementById("zoomWindow");
        var picture = document.getElementById("activeImage");
        picture.removeAttribute("id");
        element.parentNode.removeChild(element);
        activeZoom = false;
	}
	else{
        image.setAttribute("id", "activeImage");
	    console.log(image);
        zoomWindow(image.src);
        activeZoom = true;
    }
}

function zoomWindow(source) {
    var newImage;
    var zoomedImage = document.createElement("div");
    var picture = document.getElementById("activeImage");
    zoomedImage.setAttribute("id", "zoomWindow");
    newImage = new Image();
    newImage.src = source;
    newImage.alt = null;
    newImage.setAttribute("onclick", "clicked(this);");
    newImage.setAttribute("id", "zoomedImage");
    zoomedImage.appendChild(newImage);
    grid.appendChild(zoomedImage);
    document.onkeydown = function (e) {
        if (activeZoom) {
            switch (e.keyCode) {
                case 37:
                    console.log("left key");
                    nextImage(0);
                    picture.removeAttribute("id");
                    break;
                case 39:
                    console.log("right key");
                    nextImage(1);
                    picture.removeAttribute("id");
                    break;
                default:
                    break;
            }
        }
    }
}

function nextImage(direction) {
    var picture = document.getElementById("activeImage");
    var parent = picture.parentNode;
    var sibling;
    if (direction == 0){
        var element = document.getElementById("zoomWindow");
        element.parentNode.removeChild(element);
        sibling = parent.previousSibling;
        console.log(sibling.firstChild.src);
        zoomWindow(sibling.firstChild.src);
        sibling.firstChild.setAttribute("id", "activeImage");
    }
    else{
        var element = document.getElementById("zoomWindow");
        element.parentNode.removeChild(element);
        sibling = parent.nextSibling;
        console.log(sibling.firstChild.src);
        zoomWindow(sibling.firstChild.src);
        sibling.firstChild.setAttribute("id", "activeImage");
    }

}
createGallery(3);
