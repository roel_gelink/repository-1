/*jslint browser: true, eqeq: true*/
/*global console */
var images = [["images/0004681.jpg", "August 2013", "Hans Gootink"], ["images/0004755.jpg", "September 2013", "Friedrich Schreuder"], ["images/0004858.jpg", "September 2013", "Hikoshi Hanato"], ["images/0004902.jpg", "June 2014", "Peter Hegre"], ["images/0006630.jpg", "July 2014", "Louis Theroux"], ["images/0004713.jpg", "August 2013", "Hans Gootink"], ["images/0004801.jpg", "September 2013", "Friedrich Schreuder"], ["images/0004860.jpg", "September 2013", "Hikoshi Hanato"], ["images/0004931.jpg", "June 2014", "Peter Hegre"], ["images/0006638.jpg", "July 2014", "Louis Theroux"], ["images/0004720.jpg", "August 2013", "Hans Gootink"], ["images/0004802.jpg", "September 2013", "Friedrich Schreuder"], ["images/0004870.jpg", "September 2013", "Hikoshi Hanato"], ["images/0004969.jpg", "June 2014", "Peter Hegre"], ["images/0006680.jpg", "July 2014", "Louis Theroux"], ["images/0004731.jpg", "August 2013", "Hans Gootink"], ["images/0004827.jpg", "September 2013", "Friedrich Schreuder"], ["images/0004874.jpg", "September 2013", "Hikoshi Hanato"], ["images/0004971.jpg", "June 2014", "Peter Hegre"], ["images/0006743.jpg", "July 2014", "Louis Theroux"], ["images/0004750.jpg", "August 2013", "Hans Gootink"], ["images/0004853.jpg", "September 2013", "Friedrich Schreuder"], ["images/0004888.jpg", "June 2014", "Peter Hegre"], ["images/0006598.jpg", "July 2014", "Louis Theroux"], ["images/0006787.jpg", "August 2013", "Hans Gootink"] ];
var grid;
var activeZoom;
var gridnumber;

/**
 * The gallery object creates, fills and controls the gallery. It also initializes the first selected image.
 * @type {Object}
 */
var Gallery = {
/**
 * [selects the first imported image]
 * @return {[null]} [selects the first imported image]
 */
    selectInit: function () {
        "use strict";
        var firstImage = document.getElementsByTagName("img")[0];
        firstImage.setAttribute("id", "selected");
        gridnumber = document.getElementsByTagName("img").length;
    },
/**
 * [Creates a main div, and fills it with containers. These containers get filled with images using ]
 * @return {[type]} [description]
 */
    create: function () {
        "use strict";
        var i, container, grid = document.createElement("div");
        grid.setAttribute("id", "grid");
        grid.setAttribute("class", "grid5");
        document.body.appendChild(grid);
        document.getElementById("grid").style.width = screen.width / 3 + 10 + "px";
        document.getElementById("grid").style.marginLeft = -(screen.width / 3 + 10) / 2 + "px";
        window.grid = grid;
        for (i = 0; i < images.length; i += 1) {
            container = document.createElement("div");
            container.setAttribute("class", "container");
            grid.appendChild(container);
            this.importImage(images[i][0], null, i);
        }
    },
/**
 * [Creates an image element and fills it with a source from images[], also adds the onclick event to images.
 * This adds html data to the images aswell]
 * @param  {[type]} src    [source of the image]
 * @param  {[type]} alt    [alt]
 * @param  {[type]} number [used for the loop in create(), resulting in a different image]
 * @return {[type]}        [null]
 */
    importImage: function (src, alt, number) {
        "use strict";
        var element, newImage;
        newImage = new Image();
        newImage.src = src;
        newImage.alt = alt;
        newImage.setAttribute("onclick", "Interaction.zoom(this);");
        newImage.setAttribute("oncontextmenu", "event.preventDefault(); Interaction.remove(this);");
        newImage.setAttribute("data-date", images[number][1]);
        newImage.setAttribute("data-name", images[number][2]);
        element = document.getElementsByClassName("container")[number];
        element.appendChild(newImage);
    },
/**
 * [galleryControl changes the style of the grid according to how many images are in the document]
 * @return {[type]} [returns nothing]
 */
    galleryControl: function () {
        "use strict";
        if (gridnumber <= 25 && gridnumber > 16) {
            console.log("5x5");
        } else if (gridnumber <= 16 && gridnumber > 9) {
            grid.setAttribute("class", "grid4");
            console.log("4x4");
        } else if (gridnumber <= 9 && gridnumber > 4) {
            grid.setAttribute("class", "grid3");
            console.log("3x3");
        } else if (gridnumber <= 4 && gridnumber > 1) {
            grid.setAttribute("class", "grid2");
            console.log("2x2");
        }

    }
};
/**
 * [Interaction includes all the functions that enable the user to modify the page. They are all linked to the events on the document]
 * @type {Object}
 */
var Interaction = {
/**
 * [zoom creates a window that lays over the document, it fills this window with a picture and its data. It also gives this picture an onlick event]
 * @param  {[type]} image [The image that has been clicked will be given to this function]
 * @return {[type]}       [-]
 */
    zoom: function (image) {
        "use strict";
        var previousZoom = document.getElementById("selected"), newImage, zoomedWindow = document.createElement("div"), dataDate = document.createElement("p"), dataName = document.createElement("p");
        if (previousZoom) {
            previousZoom.removeAttribute("id");
        }
        image.setAttribute("id", "selected");
        console.log(image);
        activeZoom = true;
        zoomedWindow.setAttribute("id", "zoomWindow");
        newImage = new Image();
        newImage.src = image.src;
        newImage.alt = null;
        newImage.setAttribute("onclick", "Interaction.zoomOut(this);");
        newImage.setAttribute("id", "zoomedImage");
        dataDate.innerHTML = image.getAttribute("data-date");
        dataName.innerHTML = image.getAttribute("data-name");
        zoomedWindow.appendChild(newImage);
        zoomedWindow.appendChild(dataDate);
        zoomedWindow.appendChild(dataName);
        document.body.appendChild(zoomedWindow);
    },
/**
 * [zoomOut is activated by clicking on a zoomed-in picture. zoomOut removes the window]
 * @return {[type]} [-]
 */
    zoomOut: function () {
        "use strict";
        var element = document.getElementById("zoomWindow");
        element.parentNode.removeChild(element);
        activeZoom = false;
    },
/**
 * [previousImage listens to the event when a left arrowkey is pressed. Depending if a picture is zoomed in, it selects the previous image and zooms in on it]
 * @return {[type]} [description]
 */
    previousImage: function () {
        "use strict";
        var selected = document.getElementById("selected"), parent = selected.parentNode, sibling = parent.previousSibling, list = document.getElementsByTagName("img"), lastImageZoom = list[list.length - 2], lastImage = list[list.length - 1], element = document.getElementById("zoomWindow");
        if (activeZoom) {
            element.parentNode.removeChild(element);
            if (selected.parentNode == grid.firstChild) {
                selected.removeAttribute("id");
                lastImage.setAttribute("id", "selected");
                this.zoom(lastImageZoom);
            } else {
                sibling.firstChild.setAttribute("id", "selected");
                this.zoom(sibling.firstChild);
                selected.removeAttribute("id");
            }
        } else {
            if (selected.parentNode == grid.firstChild) {
                selected.removeAttribute("id");
                lastImage.setAttribute("id", "selected");
            } else {
                sibling.firstChild.setAttribute("id", "selected");
                selected.removeAttribute("id");
            }
        }
    },
/**
 * [nextImage listens to the event when a right arrowkey is pressed. Depending if a picture is zoomed in, it selects the next image and zooms in on it]
 * @return {[type]} [-]
 */
    nextImage: function () {
        "use strict";
        var selected = document.getElementById("selected"), parent = selected.parentNode, sibling = parent.nextSibling, firstImage = document.getElementsByTagName("img")[0], element = document.getElementById("zoomWindow");
        console.log(parent);
        console.log(sibling);
        if (activeZoom) {
            element.parentNode.removeChild(element);
            if (selected.parentNode == grid.lastChild) {
                selected.removeAttribute("id");
                firstImage.setAttribute("id", "selected");
                this.zoom(firstImage);
            } else {
                sibling.firstChild.setAttribute("id", "selected");
                this.zoom(sibling.firstChild);
                selected.removeAttribute("id");
            }
        } else {
            if (selected.parentNode == grid.lastChild) {
                selected.removeAttribute("id");
                firstImage.setAttribute("id", "selected");
            } else {
                sibling.firstChild.setAttribute("id", "selected");
                selected.removeAttribute("id");
            }
        }
    },
/**
 * [remove removes an image]
 * @param  {[type]} image [The given image to remove]
 * @return {[type]}       [description]
 */
    remove: function (image) {
        "use strict";
        var container = image.parentNode;
        container.parentNode.removeChild(container);
        gridnumber = gridnumber - 1;
        console.log(gridnumber);
        Gallery.galleryControl();
    }
};
/**
 * [Itinializes the website and adds the events to the DOM]
 * @return {[type]} [description]
 */
var readyStateCheckInterval = setInterval(function () {
    "use strict";
    if (document.readyState === "complete") {
        clearInterval(readyStateCheckInterval);
        Gallery.create();
        Gallery.selectInit();
        window.onkeydown = function (e) {
            switch (e.keyCode) {
            case 39:
                Interaction.nextImage();
                break;
                // Left
            case 37:
                Interaction.previousImage();
                break;
                // Space
            case 32:
                var selected = document.getElementById("selected");
                if (activeZoom) {
                    Interaction.zoomOut();
                } else {
                    Interaction.zoom(selected);
                }
                break;
            default:
                break;
            }
        };
    }
}, 10);