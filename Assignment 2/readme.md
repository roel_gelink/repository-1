
Contents
======

- Files
- Documentation (Objects and functions)
- Browser compatibility

Files
-----
**Javascript**

Photoviewer2.js is the main javascript file. It creates and modifies the entire document.

**CSS**

stylesheet2.css styles the document. Photoviewer2.js also modifies some styles from this file.

**HTML**

Photoviewer2.html is the HTML page which contains only the script to include the javascript file.

**Images**

All the images have been placed into a single folder.

Documentation
-------------

**Object Gallery:** creates, fills and controls the gallery. It also initializes the first selected image.

- **Function selectInit()** will select the first image in the grid, this way the functionality of the keyboard is immediately active.
- **Function create()** creates the grid by adding a div to the DOM, and filling these with div containers. These containers are then filled with images by the next function.
- **Function importImage()** creates a new image, and gives it a few event listeners. These include: onclick and oncontexmenu. It also makes sure that the images carry data.
- **Function galleryControl()** is called when an image is removed. It listens to the variable gridnumber, which changes when an image is removed. According to this number the style of the grid changes, keeping the grid square.

**Object Interaction:** includes all the functions that enable the user to modify the page. They are all linked to the events on the document

- **Function zoom()** creates a window that lays over the document, it fills this window with a picture and its data. It also gives this picture an onlick event.
- **Function zoomOut()** is activated by clicking on a zoomed-in picture. zoomOut removes the window.
- **Function previousImage()** listens to the event when a left arrowkey is pressed. Depending if a picture is zoomed in, it selects the previous image and zooms in on it.
- **Function nextImage()** listens to the event when a right arrowkey is pressed. Depending if a picture is zoomed in, it selects the next image and zooms in on it.
- **Function remove()** removes an image.

Browser compatibility
---------------------

Compatible with Firefox 33+ or Chrome 38+ (these were tested)