function CellModel(data, i) {
    this.innerHTML = undefined;
    this.prototype = new window.StockModel(data, i);
}

CellModel.prototype.setCellData = function (property) {
    this.innerHTML = this.prototype.getStockData(property);
};

CellModel.prototype.getCellData = function () {
    return this.innerHTML;
};

