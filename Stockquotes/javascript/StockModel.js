function StockModel(data, i) {
    this.stockData = data.query.results.row[i];
}

StockModel.prototype.getStockData = function (property) {
    return this.stockData[property];
};