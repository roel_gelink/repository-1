describe("Stock quotes tests", function () {
    var oldBodyNode;

    beforeEach(function () {
        window.crud.init(window.crud);
        oldBodyNode = document.getElementById('stockData');
        window.crud.useTestData(window.crud);
    });

    afterEach(function () {
        for (var i = 1; i < 99999; i++) { //clears interval
            window.clearInterval(i);
        }
        document.getElementById("header").parentNode.removeChild(document.getElementById("header"));
        document.getElementById("stockData").parentNode.removeChild(document.getElementById("stockData"));
        document.getElementById("msg").parentNode.removeChild(document.getElementById("msg"));
    });

    it("test createNodes", function () {
        expect(document.getElementById('stockData')).not.toEqual(null);
    });

    it("test createMsgBox", function () {
        expect(document.getElementById('msg')).not.toEqual(null);
    });

    it("test createMessage", function () {
        window.crud.createMessage("test");
        expect(document.getElementById('msg').childNodes[0]).not.toEqual(null);
    });

    it("test createMenu", function () {
        expect(document.getElementById('header')).not.toEqual(null);
    });

    it("test createTableHeader", function () {
        expect(document.getElementById('tableHeader')).not.toEqual(null);
    });

    it("test createTableContent", function () {
        expect(document.getElementById('BCS')).not.toEqual(null);
    });

    it("test updateNodes", function () {
        expect(document.getElementsByTagName('body')[0]).not.toEqual(oldBodyNode);
    });

    it("test correctReceivedData", function () {
        var testingData = window.data;
        testingData.query.results.row[0].col1 = (testingData.query.results.row[0].col1 * (Math.random() * (1.1 - 0.9 + 1)) + 0.9).toFixed(2);
        var testingDataTxt = JSON.stringify(testingData);
        testingData = window.crud.correctReceivedData(testingData);
        var correctedTestingDataTxt = JSON.stringify(testingData);

        expect(testingDataTxt).not.toEqual(correctedTestingDataTxt);
    });

    it("test getGet", function () {
        expect(window.crud.getGet()).not.toEqual(null);
    });

//    it("test createCORSRequest", function () {                        //had met andere studenten gesproken
//        expect(window.crud.createCORSRequest()).not.toEqual(null);    /en die zeiden dat deze functies niet hoeven worden getest
//    });
//
//    it("test createXMLHttpRequest", function () {
//        expect(window.crud.request).not.toEqual(null);
//    });

    it("test useTestData", function () {
        expect(document.getElementsByTagName('body')[0]).not.toEqual(oldBodyNode);
    });

    it("test init", function () {                                       //moet ik deze functie testen?
        expect(document.getElementById('BCS')).not.toEqual(null);
    });

//    it("test make random", function () {
//        var fallback = clone(crud.fallbackData);
//        crud.makeRandom();
//        expect(fallback.query.results.row[0].col1).not.toEqual(crud.fallbackData.query.results.row[0].col1);
//    });
//
//    it("test random data", function () {
//        expect(document.getElementsByClassName('table')[0]).not.toEqual(null);
//
//    });
//
//    it("test init", function () {
//        expect(document.getElementsByName('loading')).not.toEqual(null);
//        // expect(stockQuotes.loading).toEqual(null);
//
//    });

    /**
     * safe copy by value, not by reference
     * not my responsibility
     * @param obj
     * @returns {Object}
     */
    function clone(obj) {
        if (obj === null || typeof(obj) !== 'object') {
            return obj;
        }
        var temp = new obj.constructor();
        for (var key in obj) {
            temp[key] = clone(obj[key]);
        }
        return temp;
    }
});
